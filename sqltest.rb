require 'tiny_tds'
server = 'demouwine.database.windows.net'
database = 'sample'
username = 'uwine01'
password = 'itsolution1!@#qwe'
client = TinyTds::Client.new username: username, password: password, host: server, port: 1433, database: database, azure: true

puts "Reading data from table"
tsql = "SELECT TOP 20 pc.Name as CategoryName, p.name as ProductName
        FROM [SalesLT].[ProductCategory] pc
        JOIN [SalesLT].[Product] p
        ON pc.productcategoryid = p.productcategoryid"
result = client.execute(tsql)
result.each do |row|
    puts row
end
