class AirplanesController < ApplicationController
# rest api

  def getAirpalneState

    username = 'ss4483'
    authHeader = Base64.strict_encode64("#{username}:#{@@flightaware_key}")

    fxmlUrl = 'https://flightxml.flightaware.com/json/FlightXML3'
    if @@status =="DEBUG"
      file = File.read('a.json')
      parsed = JSON.parse(file)

    elsif @@status =="PRODUCTION"
      my_params = {
      	:ident => params[:flightnum].upcase ,
        :howMany => 1
      }
      response = RestClient.get "#{fxmlUrl}/FlightInfoStatus", {params: my_params, Authorization: "Basic #{authHeader}"}

      if response.code == 200
      	parsed = JSON.parse(response.body)
      else
      	parsed = JSON.parse('{  }')
      end
    end

    if parsed["FlightInfoStatusResult"] == nil
      status = nil
      ident = nil
      ori_airport_name = nil
      des_airport_name = nil
      estimated_departure_time = nil
      estimated_arrival_time = nil
      faFlightID = nil
      @tailnumber = params[:flightnum].upcase
    else
      status = parsed["FlightInfoStatusResult"]["flights"][0]["status"]
      ident = parsed["FlightInfoStatusResult"]["flights"][0]["ident"]
      ori_airport_name = parsed["FlightInfoStatusResult"]["flights"][0]["origin"]["airport_name"] # 출발 공항
      des_airport_name = parsed["FlightInfoStatusResult"]["flights"][0]["destination"]["airport_name"] # 도차 공항
      estimated_departure_time = parsed["FlightInfoStatusResult"]["flights"][0]["estimated_departure_time"] # 출발 예정 시간
      estimated_arrival_time = parsed["FlightInfoStatusResult"]["flights"][0]["estimated_arrival_time"] # 도착 예정 시간


      faFlightID = parsed["FlightInfoStatusResult"]["flights"][0]["faFlightID"]
      @tailnumber = parsed["FlightInfoStatusResult"]["flights"][0]["tailnumber"]
    end
    if @@status =="DEBUG"
      file = File.read('b.json')
      parsed = JSON.parse(file)
    elsif @@status =="PRODUCTION"
      # 현재 위치 찾기
      p !faFlightID.nil?
      if !faFlightID.nil?
        my_params_2 = {
          :ident => faFlightID
        }

        response = RestClient.get "#{fxmlUrl}/GetFlightTrack", {params: my_params_2, Authorization: "Basic #{authHeader}"}
        if response.code == 200
        	parsed = JSON.parse(response.body)
        else
        	parsed = JSON.parse('{  }')
        end
      else
        parsed = JSON.parse('{ }')
      end
    end

    if parsed["GetFlightTrackResult"] == nil
      timestamp = nil
      latitude = nil
      longitude = nil
    else
      timestamp = parsed["GetFlightTrackResult"]["tracks"][0]["timestamp"]
      latitude = parsed["GetFlightTrackResult"]["tracks"][0]["latitude"]
      longitude = parsed["GetFlightTrackResult"]["tracks"][0]["longitude"]
    end

    server = 'altdb.database.windows.net'
    database = 'Airplanes'
    username = 'uwine01'
    password = 'itsolution1!@#qwe'
    client = TinyTds::Client.new username: username, password: password, host: server, port: 1433, database: database, azure: true



    tsql = "SELECT * FROM states WHERE tailnumber = '#{@tailnumber}'"
    result = client.execute(tsql)
  

    finish_result = { status: status, ident: ident, ori_airport: ori_airport_name, des_airport: des_airport_name, estimated_departure_time: estimated_departure_time, estimated_arrival_time: estimated_arrival_time,
                      tracks: {timestamp: timestamp, latitude: latitude, longitude: longitude },
                      altdb: result.first }


    render json: finish_result


    ##########################################################################################################
    # callsign = params[:callsign] + "  "
    # # opensky 사용
    # source = "https://opensky-network.org/api/states/all?icao24"
    # resp = Net::HTTP.get_response(URI.parse(source))
    # data = resp.body
    # all = JSON.parse(data)
    #
    # item = all["states"].find  { |_, x| x == callsign }
    # if item == nil
    #   result = { state: nil }
    # else
    #   state = Hash.new
    #   state[:icao24] = item[0]
    #   state[:callsign] = item[1]
    #   state[:time_position] = item[2]
    #   state[:time_position] = item[3]
    #   state[:time_velocity] = item[4]
    #   state[:longitude] = item[5]
    #   state[:latitude] =  item[6]
    #   state[:altitude] =  item[7]
    #   state[:heading] =  item[10]
    #   p state
    #   result = { state: state}
    # end
    #
    # url = "http://openapi.airport.co.kr/service/rest/FlightStatusList/getFlightStatusList?serviceKey="
    # apikey = "1p8pMwGNkKon0Z%2FpnFOg8HW94vhO1lb9ScO36zkICyOVtJv0%2BLoa34gzgFVAteDRhI9YQRp3zhvpt5UoxdWg2g%3D%3D"
    # url = url + apikey
    # xml_doc = Nokogiri::HTML(open(url))
    # totalCount = xml_doc.xpath("//totalcount").text
    # full_url = url+"&numOfRows="+ totalCount.to_s
    #
    # xml_doc = Net::HTTP.get_response(URI.parse(full_url)).body
    # json_doc = Hash.from_xml(xml_doc).to_json
    # all = JSON.parse(json_doc)
    # items = all["response"]["body"]["items"]
    #
    #
    # result.merge!({ real: items["item"].detect{|hash| hash["airFln"] == params[:callsign]}})
    # render :json=>  result
    ##########################################################################################################


  end


  # 국내선 스케쥴
  def getAirplaneDSchedules
    url = "http://openapi.airport.co.kr/service/rest/FlightScheduleList/getDflightScheduleList?serviceKey="
    schDate	= params[:schDate]# 검색일자
    schDeptCityCode	= params[:schDeptCityCode]# 도착 도시 코드
    schArrvCityCode	= params[:schArrvCityCode]# 출항 도시 코드
    schAirLine	= params[:schAirLine]    # 항공사 코드
    # schFlightNum	= params[:schFlightNum]# 항공편 넘버
    if schAirLine == nil
      full_url = url + @@data_key + "&schDate=" + schDate + "&schDeptCityCode=" + schDeptCityCode + "&schArrvCityCode=" +schArrvCityCode
    else
      full_url = url + @@data_key + "&schDate=" + schDate + "&schDeptCityCode=" + schDeptCityCode + "&schArrvCityCode=" +schArrvCityCode + "&schAirLine" +schAirLine
    end
      #  + "&schAirLine=" +schAirLine+ "&schFlightNum=" + schFlightNum # 항공사, 항공편은 무시
  # 노코기리 설치
    xml_doc = Nokogiri::HTML(open(full_url))
    totalCount = xml_doc.xpath("//totalcount").text

    full_url = full_url+"&numOfRows="+ totalCount.to_s

    xml_doc = Net::HTTP.get_response(URI.parse(full_url)).body
    json_doc = Hash.from_xml(xml_doc).to_json
    all = JSON.parse(json_doc)

    items = all["response"]["body"]["items"]

    # json 데이터 수정 및 조합
########################################################################################################################

    wday = Date.parse(schDate).wday
    case wday
    when 1
      @wday = "domesticMon"
    when 2
      @wday = "domesticTue"
    when 3
      @wday = "domesticWed"
    when 4
      @wday = "domesticThu"
    when 5
      @wday = "domesticFri"
    when 6
      @wday = "domesticSat"
    when 7
      @wday = "domesticSun"
    end

    results = Array.new
    if items == nil
      render :json => { count: 0, schedule: [] }
    else
      items["item"].select{|hash| hash[@wday] == "Y"}.each do |i|
        result = Hash.new
        result[:airlineKorean] = i["airlineKorean"]
        result[:domesticNum] = i["domesticNum"]
        result[:day] = schDate
        result[:startcity] = i["startcity"]
        result[:domesticStartTime] = i["domesticStartTime"]
        result[:arrivalcity] = i["arrivalcity"]
        result[:domesticArrivalTime] = i["domesticArrivalTime"]
        results << result
      end


      render :json => { schedule: results}
    end
  end

  # 국제선
  def getAirplaneISchedules
    url = "http://openapi.airport.co.kr/service/rest/FlightScheduleList/getIflightScheduleList?ServiceKey="
    schDate	= params[:schDate]# 검색일자
    schDeptCityCode	= params[:schDeptCityCode]# 도착 도시 코드
    schArrvCityCode	= params[:schArrvCityCode]# 출항 도시 코드
    airlineKorean	= params[:airlineKorean]    # 항공사 코드

    if airlineKorean == nil
      full_url = url + @@data_key + "&schDate=" + schDate + "&schDeptCityCode=" + schDeptCityCode + "&schArrvCityCode=" +schArrvCityCode
    else
      full_url = url + @@data_key + "&schDate=" + schDate + "&schDeptCityCode=" + schDeptCityCode + "&schArrvCityCode=" +schArrvCityCode + "&airlineKorean" +airlineKorean
    end
    totalCount = Nokogiri::HTML(open(full_url)).xpath("//totalcount").text
    full_url = full_url+"&numOfRows="+totalCount.to_s
    all = JSON.parse(Hash.from_xml(Net::HTTP.get_response(URI.parse(full_url)).body).to_json)


    items = all["response"]["body"]["items"]

    wday = Date.parse(schDate).wday
    case wday
    when 1
      @wday = "internationalMon"
    when 2
      @wday = "internationalTue"
    when 3
      @wday = "internationalWed"
    when 4
      @wday = "internationalThu"
    when 5
      @wday = "internationalFri"
    when 6
      @wday = "internationalSat"
    when 7
      @wday = "internationalSun"
    end


    results = Array.new

    if items == nil
      render :json => { count: 0, schedule: [] }
    else
      items["item"].select{|hash| hash[@wday] == "Y"}.each do |i|
        result = Hash.new
        result[:airlineKorean] = i["airlineKorean"]
        result[:day] = schDate
        result[:airport] = i["airport"]
        result[:city] = i["city"]
        result[:internationalIoType] = i["internationalIoType"]
        result[:internationalTime] = i["internationalTime"]
        result[:internationalNum] = i["internationalNum"]
        results << result
      end
      render :json => { schedule: results}
    end
  end

  def DAirportName
      url = "http://openapi.airport.co.kr/service/rest/AirportCodeList/getAirportCodeList?serviceKey=" + @@data_key
      totalCount = Nokogiri::HTML(open(url)).xpath("//totalcount").text
      url = url+"&numOfRows=" + totalCount.to_s
      all = JSON.parse(Hash.from_xml(Net::HTTP.get_response(URI.parse(url)).body).to_json)
      items = all["response"]["body"]["items"]

      cityKor = ["서울/김포", "인천", "부산/김해", "대구", "청주", "제주", "무안", "양양", "울산", "광주", "포항", "여수", "사천", "군산", "횡성/원주"]
      resultKor = Array.new
      items["item"].select{|hash| cityKor.include?(hash["cityKor"])}.each do |i|
        resultKor.push(i)
      end

      render :json => resultKor
  end

  def IAirportName
      url = "http://openapi.airport.co.kr/service/rest/AirportCodeList/getAirportCodeList?serviceKey=" + @@data_key
      totalCount = Nokogiri::HTML(open(url)).xpath("//totalcount").text
      url = url+"&numOfRows=" + totalCount.to_s
      all = JSON.parse(Hash.from_xml(Net::HTTP.get_response(URI.parse(url)).body).to_json)
      items = all["response"]["body"]["items"]

      result = items["item"]

      render :json => result
  end
end
