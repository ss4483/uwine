class VesselsController < ApplicationController

  def getVessel
    server = 'altdb.database.windows.net'
    database = 'Vessels'
    username = 'uwine01'
    password = 'itsolution1!@#qwe'
    client = TinyTds::Client.new username: username, password: password, host: server, port: 1433, database: database, azure: true
    name_kr = params[:name_kr]
    tsql = "SELECT * FROM states WHERE name_kr = N'#{name_kr}'"
    result = client.execute(tsql)

    finish_result = Hash.new
    finish_result[:alt] = result.first

    mmsi = finish_result[:alt]["mmsi"].to_i

    if @@status =="DEBUG"
      if mmsi == nil
        finish_result[:states] = nil
      else
        file = File.read('c.json')
        parsed = JSON.parse(file)
        finish_result[:states] = parsed.first
      end
    elsif @@status =="PRODUCTION"
      # db에 mmsi 저장, 선박 이름으로 mmsi구해서 api에서 찾기
      if mmsi == nil
        finish_result[:states] = nil
      else
        url = "http://services.marinetraffic.com/api/exportvessel/v:5/"+ @@marinetraffic_key +"/timespan:60/protocol:jsono/mmsi:" + mmsi.to_s
        response = RestClient.get url
        parsed = JSON.parse(response.body)
        finish_result[:states] = parsed.first
      end
    end
    render :json => finish_result
  end

  def getShipNode
    server = 'altdb.database.windows.net'
    database = 'Vessels'
    username = 'uwine01'
    password = 'itsolution1!@#qwe'
    client = TinyTds::Client.new username: username, password: password, host: server, port: 1433, database: database, azure: true
    name_kr = params[:name_kr]
    tsql = "SELECT * FROM shipNode"
    result = client.execute(tsql)
    shipNode = Hash.new
    result. each do |node|
      shipNode["#{node["NODENAME"]}"] = node["NODEID"]
    end
    render :json => shipNode
  end

  def getVesselName
    server = 'altdb.database.windows.net'
    database = 'Vessels'
    username = 'uwine01'
    password = 'itsolution1!@#qwe'
    client = TinyTds::Client.new username: username, password: password, host: server, port: 1433, database: database, azure: true
    name_kr = params[:name_kr]
    tsql = "SELECT name_kr FROM states WHERE mmsi IS NOT NULL;"
    result = client.execute(tsql)
    ship_name = Array.new
    result.each do |name|
      ship_name << name["name_kr"]
    end
    render :json => ship_name
  end

  def getVesselSchedule
    url = "http://openapi.tago.go.kr/openapi/service/DmstcShipNvgInfoService/getShipOpratInfoList?serviceKey="
    depPlandTime	= params[:depPlandTime]# 검색일자
    arrNodeId	= params[:arrNodeId]# 도착 도시 코드
    depNodeId	= params[:depNodeId]# 출항 도시 코드
    if arrNodeId == nil
      full_url = url + @@data_key + "&depPlandTime=" + depPlandTime + "&depNodeId=" + depNodeId
    elsif depNodeId == nil
      full_url = url + @@data_key + "&depPlandTime=" + depPlandTime + "&arrNodeId=" + arrNodeId
    else
      full_url = url + @@data_key + "&depPlandTime=" + depPlandTime + "&arrNodeId=" + arrNodeId + "&depNodeId=" + depNodeId
    end

    totalCount = Nokogiri::HTML(open(full_url)).xpath("//totalcount").text
    full_url = full_url+"&numOfRows="+totalCount.to_s
    all = JSON.parse(Hash.from_xml(Net::HTTP.get_response(URI.parse(full_url)).body).to_json)

    if totalCount == "0"
      render :json => { schedule: []}
    elsif totalCount == "1"
      item = all["response"]["body"]["items"]["item"]
      p totalCount
      render :json => { schedule: [ item ] }
    else
      item = all["response"]["body"]["items"]["item"]
      render :json => { schedule: item }
    end
  end

  def getSafeInfo
    server = 'altdb.database.windows.net'
    database = 'Vessels'
    username = 'uwine01'
    password = 'itsolution1!@#qwe'
    client = TinyTds::Client.new username: username, password: password, host: server, port: 1433, database: database, azure: true
    tsql = "SELECT * FROM safeInfo"
    result = client.execute(tsql)

    render :json => result
  end
end
