class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  require 'net/http'
  require 'net/https'
  require 'nokogiri'
  require 'open-uri'
  require 'tiny_tds'
  require 'rubygems'
  require 'json'
  require 'rest-client'
  require 'base64'
  @@status = "PRODUCTION"
  # @@status = "DEBUG"
  @@data_key = "1p8pMwGNkKon0Z%2FpnFOg8HW94vhO1lb9ScO36zkICyOVtJv0%2BLoa34gzgFVAteDRhI9YQRp3zhvpt5UoxdWg2g%3D%3D"
  @@flightaware_key = "9d22e5afbaafdf7934855c2b573ac0853d066b7d"
  # @@marinetraffic_key = "c90515a0a86ed601fb31a175310d49d1d69e5454"
  @@marinetraffic_key ="8f8042ea19641312ce631cf4ca9c1eb3f0365942"
end
