Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'webs#index'
# web 뷰
  get "webs/air_result"
  get "webs/index"



# 항공기 json
# 실시간 위치, 실시간 상태
# 항공 스케쥴 - 국내 스케쥴, 국제 스케쥴
  # 한 대 실시간 위치 + 실시간 상태
  get "airplanes/States" => "airplanes#getAirpalneState"

  # 운항 스케쥴
    # 국내 스케쥴
    get "airplanes/DSchedules" => "airplanes#getAirplaneDSchedules"
    # 국제 스케쥴
    get 'airplanes/ISchedules' => "airplanes#getAirplaneISchedules"

    get 'airplanes/DAirportName'
    get 'airplanes/IAirportName'

# 선박

  get 'vessels/getVessel'=> "vessels#getVessel"
  get 'vessels/getVesselName'
  get 'vessels/getShipNode'
  get 'vessels/getVesselSchedule'
  get 'vessels/getSafeInfo'


end
